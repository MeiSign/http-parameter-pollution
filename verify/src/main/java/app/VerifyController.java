package app;

import app.domain.Transaction;
import app.domain.TransactionAction;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.ResponseEntity;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import app.exception.BadRequestException;

@RestController
public class VerifyController {

    @Value("${paymenturl}")
    private String paymentUrl;

    @PostMapping("/")
    public String res(@RequestParam String action, @RequestParam String amount) {
        try {
            Transaction transaction = new Transaction(action, amount);
            if (transaction.getAction().equals(TransactionAction.TRANSFER)) {
                System.out.println("Verify Controller: Going to transfer $"+ transaction.getAmount());
                RestTemplate restTemplate = new RestTemplate();
                UriComponents fakePaymentUrl = UriComponentsBuilder
                        .fromPath(this.paymentUrl)
                        .queryParam(action, transaction.getAction())
                        .queryParam(amount, transaction.getAmount())
                        .build();
                  //Internal fake payment micro-service
                ResponseEntity<String> response
                    = restTemplate.getForEntity(
                            fakePaymentUrl.toUriString(),
                            String.class);
                return response.getBody();
            } else if (transaction.getAction().equals(TransactionAction.WITHDRAW)) {
                return "Verify Controller: Sorry, you can only make transfer";
            } else {
                return "Verify Controller: You must specify action: transfer or withdraw";
            }
        } catch(RuntimeException ex) {
            throw new BadRequestException();
        }
    }

}
