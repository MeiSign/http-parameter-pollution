package app.domain;

public enum TransactionAction {
    WITHDRAW,
    TRANSFER
}
