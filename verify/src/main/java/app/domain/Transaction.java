package app.domain;

import java.math.BigInteger;

public class Transaction {

    private final TransactionAction action;
    private final BigInteger amount;

    public Transaction(String action, String amount) {
        if (action == null) {
            throw new IllegalArgumentException("Transaction action can't be null.");
        }

        this.action = TransactionAction.valueOf(action.toUpperCase());

        try {
            this.amount = new BigInteger(amount);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("amount is not a number");
        }

        if (this.amount.compareTo(new BigInteger("0")) < 0) {
            throw new IllegalArgumentException("Transaction amount can't be negative.");
        }

        if (this.amount.compareTo(new BigInteger("1000000")) > 0) {
            throw new IllegalArgumentException("Transaction amount can't be higher than 1000000.");
        }

    }

    public TransactionAction getAction() {
        return action;
    }

    public BigInteger getAmount() {
        return amount;
    }
}
